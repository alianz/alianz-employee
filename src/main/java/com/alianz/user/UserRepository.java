package com.alianz.user;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Integer> {

	@Query(value = "SELECT u FROM User u WHERE u.username = :#{#param.username} AND u.password = :#{#param.password}")
	public List<User> findUserByUsernamePassword(@Param("param") User user);
	
}
