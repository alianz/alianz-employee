package com.alianz.configuration;

import java.io.IOException;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.AuthenticationEntryPoint;

import com.alianz.shared.constant.ResponseCode;
import com.alianz.shared.model.ResponseBody;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			org.springframework.security.core.AuthenticationException authException)
			throws IOException, ServletException {
		ResponseBody responseBody = new ResponseBody(ResponseCode.NOT_AUTH, "Unauthorize");
		
		response.setContentType("application/json;charset=UTF-8");
		response.setStatus(403);
		response.getOutputStream().println(objectMapper.writeValueAsString(responseBody));
	}
}