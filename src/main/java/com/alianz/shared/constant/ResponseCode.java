package com.alianz.shared.constant;

public enum ResponseCode {
	SUCCESS("20000"),
	NOT_FOUND("40000"),
	NOT_AUTH("40300"),
	FAIL("50000");
	
	public final String label;
	
	private ResponseCode(String label) {
		this.label = label;
	}
}