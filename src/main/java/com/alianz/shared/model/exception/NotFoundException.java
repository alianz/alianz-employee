package com.alianz.shared.model.exception;

public class NotFoundException extends Exception {

	public NotFoundException() {}
	
	public NotFoundException(String message) {
        super(message);
    }
}
