package com.alianz.shared.model;

import com.alianz.shared.constant.ResponseCode;

public class ResponseBody {

	private String responseCode;
	private Object responseBody;
	
	public ResponseBody() {}
	
	public ResponseBody(ResponseCode responseCode, Object responseBody) {
		super();
		this.responseCode = responseCode.label;
		this.responseBody = responseBody;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(ResponseCode responseCode) {
		this.responseCode = responseCode.label;
	}

	public Object getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}
	
}
