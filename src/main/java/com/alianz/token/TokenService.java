package com.alianz.token;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import com.alianz.shared.model.exception.NotFoundException;
import com.alianz.user.User;
import com.alianz.user.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class TokenService {

	@Autowired
	private UserService userService;
	
	public void verifyUser(User user) throws NotFoundException {
		List<User> userList = this.userService.findUser(user);
		if (userList.size() < 1) throw new NotFoundException("Username or password invalid");
	}
	
	public String getJWTToken(String username) {
		String secretKey = "767c756cf9ce4639baa0466575ee912db048bfd70acc436484a9a6713dadef19";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
		String token = Jwts.builder()
					.setId("alianzJWT")
					.setSubject(username)
					.claim("authorities", 
							grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + 60000))
					.signWith(Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey)))
					.compact();
		return "Bearer " + token;
	}
	
}
