package com.alianz.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alianz.shared.constant.ResponseCode;
import com.alianz.shared.model.ResponseBody;
import com.alianz.shared.model.exception.NotFoundException;
import com.alianz.user.User;

@RestController
@RequestMapping("token")
public class TokenController {

	@Autowired
	private TokenService tokenService;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ResponseBody> getToken(@RequestBody User user) {
		try {
			this.tokenService.verifyUser(user);	
			String token = this.tokenService.getJWTToken(user.getUsername());
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.SUCCESS, token), HttpStatus.OK);
		} catch (NotFoundException error) {
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.NOT_AUTH, error.getMessage()), HttpStatus.FORBIDDEN
					);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.FAIL, "Unexpected Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
