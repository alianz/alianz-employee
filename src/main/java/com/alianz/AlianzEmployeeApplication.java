package com.alianz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlianzEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlianzEmployeeApplication.class, args);
	}

}
