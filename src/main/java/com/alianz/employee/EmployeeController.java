package com.alianz.employee;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alianz.shared.model.ResponseBody;
import com.alianz.shared.model.exception.NotFoundException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

import com.alianz.shared.constant.ResponseCode;

@RestController
@RequestMapping("employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(method = RequestMethod.GET)
	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	public ResponseEntity<ResponseBody> findAll() {
		try {
			List<EmployeeDTO> employeeList = this.employeeService.findAll();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.SUCCESS, employeeList), HttpStatus.OK);
		} catch (NotFoundException error) {
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.NOT_FOUND, error.getMessage()), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.FAIL, "Unexpected Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	public ResponseEntity<ResponseBody> findById(@PathVariable Integer id) {
		try {
			EmployeeDTO dto = this.employeeService.findById(id);
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.SUCCESS, dto), HttpStatus.OK);
		} catch (NotFoundException error) {
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.NOT_FOUND, error.getMessage()), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.FAIL, "Unexpected Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	public ResponseEntity<ResponseBody> save(@RequestBody EmployeeDTO dto) {
		try {
			this.employeeService.save(dto);
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.SUCCESS, "Success"), HttpStatus.CREATED);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.FAIL, "Unexpected Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	public ResponseEntity<ResponseBody> update(@RequestBody EmployeeDTO dto, @PathVariable Integer id) {
		try {
			this.employeeService.update(dto, id);
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.SUCCESS, "Success"), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.FAIL, "Unexpected Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	public ResponseEntity<ResponseBody> delete(@PathVariable Integer id) {
		try {
			this.employeeService.delete(id);
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.SUCCESS, "Success"), HttpStatus.OK);
		} catch (Exception error) {
			error.printStackTrace();
			return new ResponseEntity<ResponseBody>(new ResponseBody(ResponseCode.FAIL, "Unexpected Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
