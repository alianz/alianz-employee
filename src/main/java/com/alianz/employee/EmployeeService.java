package com.alianz.employee;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alianz.shared.model.exception.NotFoundException;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public List<EmployeeDTO> findAll() throws NotFoundException {
		Iterable<Employee> employeeList = this.employeeRepository.findAll();
		List<EmployeeDTO> employeeDtoList = EmployeeMapper.INSTANCE.employeesToEmployeeDtos(employeeList);
		
		if (employeeDtoList.size() < 1) throw new NotFoundException("No employee found");
		return employeeDtoList;
	}
	
	public EmployeeDTO findById(Integer id) throws NotFoundException {
		Optional<Employee> employee = this.employeeRepository.findById(id);
		if (employee.isEmpty()) throw new NotFoundException("No employee found");
		
		EmployeeDTO dto = EmployeeMapper.INSTANCE.employeeToEmployeeDto(employee.get());
		return dto;
	}
	
	public Employee save(EmployeeDTO dto) {
		Employee employee = EmployeeMapper.INSTANCE.employeeDtoToEmployee(dto);
		return this.employeeRepository.save(employee);
	}
	
	public Employee update(EmployeeDTO dto, Integer id) {
		Employee employee = EmployeeMapper.INSTANCE.employeeDtoToEmployee(dto);
		employee.setId(id);
		return this.employeeRepository.save(employee);
	}
	
	public void delete(Integer id) {
		this.employeeRepository.deleteById(id);
	}
	
}
