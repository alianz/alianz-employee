package com.alianz.employee;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
@Transactional
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}
