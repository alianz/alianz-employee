package com.alianz.employee;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EmployeeMapper {

	EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);
	
	@Mapping(source = "name", target = "firstName")
	@Mapping(source = "lname", target = "lastName")
	Employee employeeDtoToEmployee(EmployeeDTO dto); 
	
	@Mapping(source = "firstName", target = "name")
	@Mapping(source = "lastName", target = "lname")
	EmployeeDTO employeeToEmployeeDto(Employee employee);
	
	@Mapping(source = "name", target = "firstName")
	@Mapping(source = "lname", target = "lastName")
	Iterable<Employee> employeesDtosToEmployees(List<EmployeeDTO> dtos);
	
	@Mapping(source = "firstName", target = "name")
	@Mapping(source = "lastName", target = "lname")
	List<EmployeeDTO> employeesToEmployeeDtos(Iterable<Employee> employees);
}
