package com.alianz.employee;

public class EmployeeDTO {
	private Integer id;
	private String name;
	private String lname;
	private Integer age;

	public EmployeeDTO() {
	}

	public EmployeeDTO(Integer id, String name, String lname, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.lname = lname;
		this.age = age;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [name=" + name + ", lname=" + lname + ", age=" + age + "]";
	}

}
