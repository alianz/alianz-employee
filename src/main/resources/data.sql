DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(250) NOT NULL,
    last_name VARCHAR(250) NOT NULL,
    age INT DEFAULT NULL
);

INSERT INTO employee (first_name, last_name, age) VALUES
    ('Pawarut', 'Klaiarmon', 26),
    ('Robert', 'Something', 33),
    ('Jacop', 'Anything', 40);

DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(250) NOT NULL,
    password VARCHAR(250) NOT NULL
);

INSERT INTO user (username, password) VALUES
    ('admin', 'admin');