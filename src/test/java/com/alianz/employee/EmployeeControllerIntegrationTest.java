package com.alianz.employee;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.alianz.shared.constant.ResponseCode;
import com.alianz.shared.model.ResponseBody;
import com.alianz.shared.model.exception.NotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeControllerIntegrationTest {

	@InjectMocks
	private EmployeeController employeeController;

	@Mock
	private EmployeeService employeeService;

	private Employee firstEmployee = new Employee(1, "Pawarut", "Klaiarmon", 26);
	private Employee secondEmployee = new Employee(2, "Astral", "Chain", 40);

	@BeforeEach
	public void init() throws NotFoundException {
		MockitoAnnotations.initMocks(this);

		EmployeeDTO firstEmployeeDTO = EmployeeMapper.INSTANCE.employeeToEmployeeDto(firstEmployee);
		EmployeeDTO secondEmployeeDTO = EmployeeMapper.INSTANCE.employeeToEmployeeDto(secondEmployee);

		List<EmployeeDTO> employeeDtoList = new ArrayList<EmployeeDTO>();
		employeeDtoList.add(firstEmployeeDTO);
		employeeDtoList.add(secondEmployeeDTO);

		when(this.employeeService.findAll())
			.thenReturn(employeeDtoList);
		when(this.employeeService.findById(1))
			.thenReturn(firstEmployeeDTO);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldFindAllWorkCorrectly() {
		ResponseEntity<ResponseBody> response = this.employeeController.findAll();

		assertThat(response.getStatusCode())
			.isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getResponseCode())
			.isEqualTo(ResponseCode.SUCCESS.label);
		assertThat(((List<EmployeeDTO>) response.getBody().getResponseBody()).size())
			.isEqualTo(2);
	}

	@Test
	public void shouldFindByIdWorkCorrectly() {
		ResponseEntity<ResponseBody> response = this.employeeController.findById(1);

		assertThat(response.getStatusCode())
			.isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getResponseCode())
			.isEqualTo(ResponseCode.SUCCESS.label);
		assertThat(((EmployeeDTO) response.getBody().getResponseBody()).getName())
			.isEqualTo("Pawarut");
	}

	@Test
	public void shouldSaveWorkCorrectly() {
		EmployeeDTO dto = new EmployeeDTO(null, "Mario", "Bros", 40);
		ResponseEntity<ResponseBody> response = this.employeeController.save(dto);

		assertThat(response.getStatusCode())
			.isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody().getResponseCode())
			.isEqualTo(ResponseCode.SUCCESS.label);
		assertThat(response.getBody().getResponseBody())
			.isEqualTo("Success");
	}

	@Test
	public void shouldUpdateWorkCorrectly() {
		EmployeeDTO dto = new EmployeeDTO(1, "Mario", "Bros", 40);
		ResponseEntity<ResponseBody> response = this.employeeController.update(dto, 1);

		assertThat(response.getStatusCode())
			.isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getResponseCode())
			.isEqualTo(ResponseCode.SUCCESS.label);
		assertThat(response.getBody().getResponseBody())
			.isEqualTo("Success");
	}
	
	@Test
	public void shouldDeleteWorkCorrectly() {
		ResponseEntity<ResponseBody> response = this.employeeController.delete(1);
	
		assertThat(response.getStatusCode())
			.isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().getResponseCode())
			.isEqualTo(ResponseCode.SUCCESS.label);
		assertThat(response.getBody().getResponseBody())
			.isEqualTo("Success");
	}

}
