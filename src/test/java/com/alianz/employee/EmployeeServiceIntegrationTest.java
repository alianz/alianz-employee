package com.alianz.employee;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alianz.shared.model.exception.NotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceIntegrationTest {
	@InjectMocks
	private EmployeeService employeeService;

	@Mock
	private EmployeeRepository employeeRepository;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		Employee firstEmployee = new Employee(1, "Pawarut", "Klaiarmon", 26);
		Employee secondEmployee = new Employee(2, "Astral", "Chain", 33);
		Employee thirdEmployee = new Employee(2, "Mario", "Bros", 31);
		
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(firstEmployee);
		employeeList.add(secondEmployee);
		employeeList.add(thirdEmployee);
		
		Iterable<Employee> iterable = employeeList;
		
		when(this.employeeRepository.findById(1)).thenReturn(Optional.ofNullable(firstEmployee));
		secondEmployee.setAge(40);
		when(this.employeeRepository.findById(2)).thenReturn(Optional.ofNullable(secondEmployee));
		when(this.employeeRepository.findById(3)).thenReturn(Optional.empty());
		when(this.employeeRepository.findAll()).thenReturn(iterable);
		when(this.employeeRepository.save(firstEmployee)).thenReturn(firstEmployee);
	}
	
	@Test
	public void whenVaidId_thenEmployeeShouldBeFound() throws NotFoundException {
		EmployeeDTO dto = this.employeeService.findById(1);
		
		assertThat(dto.getId())
			.isEqualTo(1);
	}
	
	@Test
	public void whenFindAll_thenAllEmployeeShouldReturn() throws NotFoundException {
		List<EmployeeDTO> employeeDTOList = this.employeeService.findAll();
		
		assertThat(employeeDTOList.size())
			.isEqualTo(3);
	}
	
	@Test
	public void whenCreate_thenHasDataInDatabase() throws NotFoundException {
		EmployeeDTO employeeDTO = new EmployeeDTO(1, "Pawarut", "Klaiarmon", 26);
		this.employeeService.save(employeeDTO);
		
		EmployeeDTO employee = this.employeeService.findById(1);
	
		assertThat(employee.getId())
			.isEqualTo(1);
	}
	
	@Test
	public void whenUpdate_thenHasUpdateDataInDatabase() throws NotFoundException {
		EmployeeDTO employeeDTO = new EmployeeDTO(2, "Astral", "Chain", 40);
		this.employeeService.save(employeeDTO);
		
		EmployeeDTO employee = this.employeeService.findById(2);
		
		assertThat(employee.getAge())
			.isEqualTo(40);
	}
	
}
