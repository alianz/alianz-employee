package com.alianz.employee;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryIntegrationTest {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Test
	public void whenFindById_thenReturnEmployee() {
		Optional<Employee> foundEmployee = this.employeeRepository.findById(1);
		
		assertThat(foundEmployee.isEmpty())
			.isEqualTo(false);
		assertThat(foundEmployee.get().getFirstName())
			.isEqualTo("Pawarut");
	}
	
	@Test
	public void whenFindAll_thenReturnAllEmployee() {
		Iterable<Employee> foundEmployee = this.employeeRepository.findAll();
		List<Employee> employeeList = new ArrayList<Employee>();
		foundEmployee.iterator().forEachRemaining(employeeList::add);
		
		assertThat(employeeList.size())
			.isEqualTo(3);
	}
	
	@Test
	public void whenInsert_thenHasDataInDatabase() {
		Employee employee = new Employee(null, "Astral", "Chain", 33);
		
		this.employeeRepository.save(employee);
		
		Optional<Employee> found = this.employeeRepository.findById(4);
		
		assertThat(found.isEmpty())
			.isEqualTo(false);
		assertThat(found.get().getFirstName())
			.isEqualTo("Astral");
	}
	
	@Test
	public void whenUpdate_thenHasUpdateDataInDatabase() {
		Employee employee = new Employee(1, "PawarutUpdate", "Klaiarmon", 26);
		
		this.employeeRepository.save(employee);
		
		Optional<Employee> found = this.employeeRepository.findById(1);
		
		assertThat(found.isEmpty())
			.isEqualTo(false);
		assertThat(found.get().getFirstName())
			.isEqualTo("PawarutUpdate");
	}
	
	public void whenDelete_thenNotHasDataInDatabase() {
		Optional<Employee> target = this.employeeRepository.findById(1);
		
		assertThat(target.isEmpty())
			.isEqualTo(false);
		
		this.employeeRepository.deleteById(1);
		
		Optional<Employee> found = this.employeeRepository.findById(1);
		
		assertThat(found.isEmpty())
			.isEqualTo(true);
	}
}
