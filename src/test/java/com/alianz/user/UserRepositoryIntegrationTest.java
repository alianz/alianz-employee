package com.alianz.user;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void whenFindByUsernameAndPassword_thenReturnUser() {
		User find = new User(null, "admin", "admin");
		List<User> userList = this.userRepository.findUserByUsernamePassword(find);
		
		assertThat(userList.size())
			.isEqualTo(1);
	}

}
