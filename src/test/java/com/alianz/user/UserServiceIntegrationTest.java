package com.alianz.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceIntegrationTest {

	@InjectMocks
	private UserService userService;
	
	@Mock
	private UserRepository userRepository;
	
	private User firstUser = new User(1, "admin", "admin");
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		
		List<User> userList = new ArrayList<User>();
		userList.add(firstUser);
		
		when(this.userRepository.findUserByUsernamePassword(firstUser))
			.thenReturn(userList);
	}
	
	@Test
	public void whenValidUsernameAndPassword_thenUserShouldBeFound() {
		List<User> found = this.userService.findUser(firstUser);
		
		assertThat(found.size())
			.isEqualTo(1);
		assertThat(found.get(0).getId())
			.isEqualTo(1);
	}
	
}
