# Alianz Employee

  - Swagger-UI url <http://localhost:8080/swagger-ui>

# How to test

  1. Start Springboot application
  2. Open Swagger-UI <http://localhost:8080/swagger-ui>
  3. Get JWT Token via [token api](http://localhost:8080/swagger-ui/index.html?configUrl=/api-docs/swagger-config#/token-controller/getToken)
  4. Revise username and password to admin
  5. Copy Bearer from response panel
  6. Go to top you will found Authorize then click and paste bearer
  7. Try any api you want